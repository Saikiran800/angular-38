import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  employees: any;
  countries: any;
  departments: any;
  emp: any;
  
  //Implementing Dependency Injection for EmpService using Constructor
  
  constructor(private service: EmpService) {
    this.emp = {empId:'', empName:'', salary:'', gender:'', country:'', doj:'', emailId:'', password:'', Department: {deptId:''}};

  }
  
  ngOnInit(){
    this.service.getAllCountries().subscribe((data: any) => {
      this.countries = data;
      console.log(data);
    });

    this.service.getDepartments().subscribe((deptData: any) => {
      this.departments = deptData;
      console.log(deptData);
    });
  }

    employeeRegistration(regFrom: any) {
      console.log(regFrom);
  
      this.emp.empId = regFrom.empId;
      this.emp.empName = regFrom.empName;
      this.emp.salary = regFrom.salary;
      this.emp.gender = regFrom.gender;
      this.emp.doj = regFrom.doj;
      this.emp.country = regFrom.country;
      this.emp.emailId = regFrom.emailId;
      this.emp.password = regFrom.password;
      this.emp.Department.deptId = regFrom.deptId;
      
      this.service.registerEmployee(this.emp).subscribe((data: any) => {
        console.log(data);
      });
    }
  



}
