import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProductsComponent } from './products/products.component';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './auth.guard';
import { LogoutComponent } from './logout/logout.component';
import { GetempbyidComponent } from './getempbyid/getempbyid.component';


const routes: Routes = [
  {path:"", component:LoginComponent},
  {path:"login", component:LoginComponent},
  {path:"register", component:RegisterComponent},
  {path:"showemp", component:ShowemployeesComponent},
  {path:"products", component:ProductsComponent},
  {path:"empbyid",  canActivate:[AuthGuard], component:GetempbyidComponent},
  {path:"logout",    canActivate:[AuthGuard], component:LogoutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
