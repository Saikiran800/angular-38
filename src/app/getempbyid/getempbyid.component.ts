import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-getempbyid',
  templateUrl: './getempbyid.component.html',
  styleUrls: ['./getempbyid.component.css']
})
export class GetempbyidComponent implements OnInit{

  
  emp: any;

  constructor(private service: EmpService) {
    
  }
  
  ngOnInit(){
  }

  getEmpById(empForm: any) {

    this.service.getEmpById(empForm.empId).subscribe((empData: any) => {
      this.emp = empData;
      console.log(empData);
    });

  }

}
