import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { ExpPipe } from './exp.pipe';
import { GenderPipe } from './gender.pipe';

import { ProductsComponent } from './products/products.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './register/register.component';
import { RouterModule } from '@angular/router';
import { LogoutComponent } from './logout/logout.component';
import { GetempbyidComponent } from './getempbyid/getempbyid.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ShowemployeesComponent,
    ExpPipe,
    GenderPipe,
    ProductsComponent,
    HeaderComponent,
    RegisterComponent,
    LogoutComponent,
    GetempbyidComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
