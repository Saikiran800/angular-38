import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  isUserLogged: boolean;

 //Implementing Dependency Injection for HttpClient using Constructor
 constructor(private http: HttpClient) { 
  this.isUserLogged = false;
}

//LoginSuccess
setUserLoggedIn() {
  this.isUserLogged = true;
}

getUserLoggedStatus(): boolean {
  return this.isUserLogged;
}

setUserLogout() {
  this.isUserLogged = false;
}

getAllCountries(): any {
  return this.http.get('https://restcountries.com/v3.1/all');
}

getAllEmployees(): any {
  return this.http.get('http://localhost:8085/getAllEmployees');
}

getEmpById(empId: any): any {
  return this.http.get('/getEmployeeById/' + empId);
}

getEmployee(loginForm: any): any {
  return this.http.get('/login/' + loginForm.emailId + "/" + loginForm.password).toPromise();
}

// setEmployee(loginForm: any): any{
//   return  this.http.get('/registerEmployee' + );
// }

deleteEmployee(empId: any): any {
  return this.http.delete('/deleteEmployee/' + empId)
}

registerEmployee(emp: any) {
  return this.http.post('/registerEmployee', emp);
}



}
