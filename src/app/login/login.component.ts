import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  employees:any;
  empId: any;
  empName: any;
  salary: any;
  gender: any;
  emailId: any;
  password: any;
  doj: any;
  employee: any;

  constructor(private router: Router,private service: EmpService){
   
  }
  
  ngOnInit(): void {
    
  }

  // LoginSubmit(){

  //   if(this.emailId=='HR'&& this.password =='password'){
  //     alert('welcome HR')
  //   }else{
  //     alert('inavlid credentials')
  //   }

  // }

  async login(loginForm: any) {
    console.log(loginForm);

    if (loginForm.emailId === 'HR' && loginForm.password === 'HR') {
      alert('Welcome to HR Home Page');
      this.service.setUserLoggedIn();
      this.router.navigate(['showemp']);

    } else {
      
      await this.service.getEmployee(loginForm).then((empData: any) => {
        this.employee = empData;
        console.log(empData);
      });

      if (this.employee != null) {
        this.service.setUserLoggedIn();
        this.router.navigate(['products']);
      } else {
        alert('Invalid Credentials');
        this.router.navigate(['login']);
      }   


    }

  }



}
